import React, {Component} from 'react';
import {ApolloProvider} from 'react-apollo';
import styled, {ThemeProvider, createGlobalStyle} from 'styled-components'
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import {Provider} from 'react-redux';
import Loadable from 'react-loadable';

import LoadingSpinner from './Components/LoadingSpinner'
import client from './ApolloClient';
import store from './store';
import constants from './constants'

import Header from "./Components/Header";

const HomeView = Loadable({
  loader: () => import('./views/HomeView'),
  loading: LoadingSpinner
});

const ApartmentView = Loadable({
  loader: () => import('./views/ApartmentView'),
  loading: LoadingSpinner
});

const SearchView = Loadable({
  loader: () => import('./views/SearchView'),
  loading: LoadingSpinner
});

const LocationsView = Loadable({
  loader: () => import('./views/LocationsView'),
  loading: LoadingSpinner
});


const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
  body {
    font-family: 'Roboto', sans-serif;
    margin: 0;
    line-height: 24px;
  }
`

const Layout = styled.div`
  max-width: 1000px;
  padding: 0 30px;
  margin: 0 auto 50px;
`

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <Provider store={store}>
          <Router>
            <ThemeProvider theme={constants.theme}>
              <Layout>
                <GlobalStyle />
                <Header />
                <Switch>
                  <Route exact path="/" component={HomeView}/>
                  <Route exact path="/apartments/:apartmentId" component={ApartmentView}/>
                  <Route exact path="/search" component={SearchView}/>
                  <Route exact path="/locations" component={LocationsView}/>
                  <Redirect to="/" />
                </Switch>
              </Layout>
            </ThemeProvider>
          </Router>
        </Provider>
      </ApolloProvider>
    );
  }
}

export default App;
