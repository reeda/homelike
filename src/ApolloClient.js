import ApolloClient from "apollo-boost/lib/index";

const client = new ApolloClient({
    uri: process.env.REACT_APP_HOME_URL + '/graphql'
});

export default client;
