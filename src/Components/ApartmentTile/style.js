import styled from 'styled-components'

export const StyleBase = styled.div`
  a {
    text-decoration: none;
  }
  &:hover {
    div:first-child {
      transform: scale(1.02);
      box-shadow: 0 4px 6px rgba(50,50,93,.11), 0 1px 3px rgba(0,0,0,.08);
    }
  }
`

export const ApartmentCover = styled.div`
  padding-bottom: 66%;
  background-image: url(${props => props.bgImg});
  background-size: cover;
  border-radius: 2px;
  transition: .15s transform ease-in;
`

export const ApartmentTitle = styled.div`
  padding-top: 5px;
  h4{
    font-size: 13px;
    color: ${props => props.theme.grey};
    font-weight: 500;
  }
  h3 {
    color: ${props => props.theme.primary};
    font-size: 26px;
    margin: 5px 0;
    &.apartment__price span {
      font-size: 13px;
    }
  }
`

export const ApartmentDetails = styled.div`
  margin-top: 10px;
`
