import React from 'react'
import { Link } from 'react-router-dom'

import ApartmentAmentity from '../ApartmentAmentity'
import { StyleBase, ApartmentCover, ApartmentTitle, ApartmentDetails } from './style'

const ApartmentTileView = (props) => {
  let { apartment } = props
  let url = '/apartments/' + apartment._id
  let image =
    process.env.REACT_APP_HOME_URL +
    '/images/apartments/' +
    apartment.images[0]

  return (
    <StyleBase>
      <Link target="_blank" to={url}>
        <ApartmentCover bgImg={image}>
        </ApartmentCover>
        <ApartmentTitle>
          <h4>{apartment.title} <span> · {apartment.size} m²</span> </h4>
          <h3 className="apartment__price">{apartment.price}€ <span>/ Month</span> </h3>
        </ApartmentTitle>
        <ApartmentDetails>
          <ApartmentAmentity amenities={apartment.amenities} />
        </ApartmentDetails>
      </Link>
    </StyleBase>
  )
}

export default ApartmentTileView
