import styled from 'styled-components'

export const StyleBase = styled.div`
  padding: 40px 0;
  margin-bottom: 70px;
  display: flex;
`

export const Navigation = styled.div`
  margin-left: auto;
  a {
    color: ${props => props.theme.secondary};
    margin-right: 40px;
    font-weight: 400;
    text-decoration: none;
    font-size: 17px;
  }
`
