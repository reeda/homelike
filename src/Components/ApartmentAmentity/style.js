import styled from 'styled-components'

export const StyleBase = styled.span`
  font-size: 12px;
  font-weight: 500;
  letter-spacing: .2px;
  color: ${props => props.theme.secondary};
  display: inline-flex;
  align-items: center;
  margin-right: 15px;
  span {
    margin-left: 10px;
    text-transform: capitalize;
  }
`
