import React from 'react';

import { StyleBase } from './style'

const ApartmentAmentity = ({ amenities, limit }) => {
  return amenities
    .filter((_, index) => index < limit)
    .map((item, index) => (
      <StyleBase key={index}>
        <img src={`/img/icon/${item}.svg`} height="18" alt={item}/>
        <span>{item}</span>
      </StyleBase>
    ))
}

ApartmentAmentity.defaultProps = {
  limit: 3
}

export default ApartmentAmentity
