import React from 'react'
import styled from 'styled-components'

const StyleBase = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  div.spinner {
    height: 28px;
    width: 28px;
    animation: rotate 0.8s infinite linear;
    border: 4px solid rgba(46, 88, 124, .2);
    border-right-color: ${props => props.theme.primary};
    border-radius: 50%;
  }
@keyframes rotate {
  0%    { transform: rotate(0deg); }
  100%  { transform: rotate(360deg); }
}
`

export const LoadingSpinner = () => {
  return (
    <StyleBase>
      <div className="spinner" />
    </StyleBase>
  )
}

export default LoadingSpinner
