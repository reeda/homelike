import React from 'react'

import { StyleBase, LabelStyle, SelectStyle } from './style'

const Select = (props) => {
  return (
    <StyleBase>
      {!!props.label && <LabelStyle htmlFor={props.id}>{props.label}</LabelStyle>}
      <SelectStyle {...props} >
        {props.children}
      </SelectStyle>
    </StyleBase>
  )
}

export default Select
