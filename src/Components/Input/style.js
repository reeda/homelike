import styled from 'styled-components'

export const StyleBase = styled.div`
  
`

export const LabelStyle = styled.label`
  font-size: 12px;
  display: block;
`

export const InputStyle = styled.input`
  border: 1px solid #DEDEDE;
  padding: 8px 12px;
  border-radius: 4px;
  width: 100%;
  font-size: 14px;
  color: ${props => props.theme.secondary};
  &:focus {
    border-color: ${props => props.theme.primary};
    outline: 0;
  }
`
