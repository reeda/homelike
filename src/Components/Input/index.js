import React from 'react'

import { StyleBase, LabelStyle, InputStyle } from './style'

const Input = (props) => {
  return (
    <StyleBase>
      {!!props.label && <LabelStyle htmlFor={props.id}>{props.label}</LabelStyle>}
      <InputStyle {...props} />
    </StyleBase>
  )
}

export default Input
