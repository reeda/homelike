import React from 'react'
import { Link } from 'react-router-dom'
import { StyleBase, Cover } from './style'

const mappedCities = {
  berlin: 'https://www.thehomelike.com/wp-content/uploads/berlin-1932468_1920-1.jpg',
  cologne: 'https://www.thehomelike.com/wp-content/uploads/cologne-cathedral-1510209_1920.jpg',
  stuttgart: 'https://www.thehomelike.com/wp-content/uploads/stuttgart-2109990.jpg'
}

const CityTile = ({ title, _id }) => {
  return (
    <StyleBase>
      <Link to={`/search?city=${title.toLowerCase()}`}>
        <Cover bgImg={mappedCities[title.toLowerCase()]} />
        <h3>{title}</h3>
      </Link>
    </StyleBase>
  )
}

export default CityTile
