import styled from 'styled-components'

export const StyleBase = styled.div`
  max-width: 320px;
  border-radius: 2px;
  overflow: hidden;
  position: relative;
  transition: .1s all ease-in;
  &:hover {
    box-shadow: 0 4px 6px rgba(50,50,93,.11), 0 1px 3px rgba(0,0,0,.08);
    transform: translateY(-2px);
  }
  a {
    text-decoration: none;
    &:after {
      content: '';
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      background-color: rgba(0,0,0,.2);
    }
  }
  h3 {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    color: #FFF;
    font-size: 30px;
    font-weight: 700;
    z-index: 1;
  }
`

export const Cover = styled.div`
  background-image: url(${props => props.bgImg});
  background-size: cover;
  display: block;
  padding-top: 60%;
`
