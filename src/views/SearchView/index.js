import React, { Component } from 'react'
import { connect } from 'react-redux'
import without from 'lodash/without'
import qs from 'qs'

import { fetchLocations } from '../../actions/locationsActions'
import LoadingSpinner from '../../Components/LoadingSpinner'
import FilteredApartments from './FilteredApartments'
import SearchFilter from './SearchFilter'

import { StyleBase, Title } from './style'

class LocationsView extends Component {
  constructor(props) {
    super(props)

    const {
      location: { search }
    } = props
    // eslint-disable-next-line
    const [_, query] = search.split('?')
    const { city = '' } = qs.parse(query)

    // stagedLocation is when users come to this page via a url such as `search?city=berlin`
    // and the reason why it is called *staged* because we don't fire the dispatcher right away
    // we need to wait till we retrieve the location then we can map the string with it's id (see Line: 82)
    this.state = {
      stagedLocation: city,
      location: '',
      size: 0,
      price: 0,
      services: [],
      amenities: [],
      rooms: 0,
      bedrooms: 0,
      bathrooms: 0
    }
  }
  componentDidMount() {
    this.props.fetchLocations()
  }

  handleInputChange = e => {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  handleToggleService = e => {
    const { name, value } = e.target
    // filter can be either `services` or `amenities`
    const { [name]: filter } = this.state

    this.setState({
      [name]: filter.includes(value)
        ? without(filter, value)
        : [...filter, value]
    })
  }

  render() {
    let { locationsList } = this.props
    let { location, stagedLocation } = this.state
    if (locationsList.loading) {
      return (
        <div style={{
          marginTop: 160
        }}>
          <LoadingSpinner />
        </div>
      )
    }
    if (locationsList.error) {
      return <div>{locationsList.error}</div>
    }

    const mappedLocation = !stagedLocation
      ? location
      : locationsList.items.find(
          item => item.title.toLowerCase() === stagedLocation
        )._id
    return (
      <StyleBase>
        <Title>Find your apartment</Title>
        <SearchFilter
          locationsList={locationsList}
          {...this.state}
          handleInputChange={this.handleInputChange}
          handleToggleService={this.handleToggleService}
          location={mappedLocation}
        />

        <FilteredApartments
          {...this.state}
          location={mappedLocation}
        />
      </StyleBase>
    )
  }
}

const mapStateToProps = state => ({
  locationsList: { ...state.locationsList }
})

export default connect(
  mapStateToProps,
  { fetchLocations }
)(LocationsView)
