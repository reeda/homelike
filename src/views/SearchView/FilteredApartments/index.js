import React, { Component } from 'react'
import { connect } from 'react-redux'
import difference from 'lodash/difference'

import ApartmentTile from '../../../Components/ApartmentTile'
import LoadingSpinner from '../../../Components/LoadingSpinner'
import { fetchApartmentsList } from '../../../actions/apartmentsListActions'

class FilteredApartments extends Component {
  componentDidMount() {
    const { location } = this.props
    this.props.fetchApartmentsList({ location })
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.props.fetchApartmentsList({ location: this.props.location })
    }
  }

  applyFilter = item => {
    const { size, price, rooms, bedrooms, bathrooms, amenities, services } = this.props

    return (
      (!size || item.size <= size) &&
      (!price || item.price <= price) &&
      item.details.rooms >= rooms &&
      item.details.bedrooms >= bedrooms &&
      item.details.bathrooms >= bathrooms &&
      difference(amenities, item.amenities).length === 0 &&
      difference(services, item.services).length === 0
    )
  }

  render() {
    let { apartmentsList } = this.props
    if (apartmentsList.loading) {
      return (
        <div style={{
          marginTop: 160
        }}>
          <LoadingSpinner />
        </div>
      )
    }
    if (apartmentsList.error) {
      return <div>{apartmentsList.error}</div>
    }

    return (
      <div className="apartment-list">
        {apartmentsList.items
          .filter(this.applyFilter)
          .map((item, index) => (
            <ApartmentTile key={index} apartment={item} />
          ))
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  apartmentsList: { ...state.apartmentsList }
})

export default connect(
  mapStateToProps,
  { fetchApartmentsList }
)(FilteredApartments)
