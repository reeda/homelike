import React from 'react'

import Input from '../../../Components/Input'
import Select from '../../../Components/Select'

import { StyledBase, FirstFilter, SecondFilter, ServicesBlock } from './style'

const SearchFilter = ({
  handleInputChange,
  handleToggleService,
  locationsList,
  size,
  price,
  rooms,
  bathrooms,
  bedrooms,
  location
}) => {
  return (
    <StyledBase>
      <FirstFilter>
        <Select label="Location" onChange={handleInputChange} value={location} name="location">
          <option value="">everywhere</option>
          {locationsList.items.map(location => (
            <option value={location._id} key={location._id}>
              {location.title}
            </option>
          ))}
        </Select>

        <Input
          id="size"
          label="Size"
          type="number"
          name="size"
          value={size}
          onChange={handleInputChange}
        />

        <Input
          id="price"
          label="Price"
          type="number"
          name="price"
          value={price}
          onChange={handleInputChange}
        />

        <Input
          id="rooms"
          label="Rooms"
          type="number"
          name="rooms"
          value={rooms}
          onChange={handleInputChange}
        />

        <Input
          id="bathrooms"
          label="Bathrooms"
          type="number"
          name="bathrooms"
          value={bathrooms}
          onChange={handleInputChange}
        />

        <Input
          id="bedrooms"
          label="Bedrooms"
          type="number"
          name="bedrooms"
          value={bedrooms}
          onChange={handleInputChange}
        />
      </FirstFilter>
      <SecondFilter>
        <ServicesBlock>
          <h3>Amenities</h3>
          <div>
            <Input
              id="service-television"
              label="Television"
              type="checkbox"
              name="amenities"
              value="television"
              onChange={handleToggleService}
            />

            <Input
              id="service-elevator"
              label="Elevator"
              type="checkbox"
              name="amenities"
              value="elevator"
              onChange={handleToggleService}
            />

            <Input
              id="service-heating"
              label="Heating"
              type="checkbox"
              name="amenities"
              value="heating"
              onChange={handleToggleService}
            />

            <Input
              id="service-cooker"
              label="Cooker"
              type="checkbox"
              name="amenities"
              value="cooker"
              onChange={handleToggleService}
            />

            <Input
              id="service-microwave"
              label="microwave"
              type="checkbox"
              name="amenities"
              value="microwave"
              onChange={handleToggleService}
            />
          </div>
        </ServicesBlock>
        <ServicesBlock>
          <h3>Services</h3>
          <div>
            <Input
              id="service-concierge"
              label="Concierge"
              type="checkbox"
              name="services"
              value="concierge"
              onChange={handleToggleService}
            />

            <Input
              id="service-cleaning"
              label="Cleaning"
              type="checkbox"
              name="services"
              value="cleaning"
              onChange={handleToggleService}
            />

            <Input
              id="service-fullFridge"
              label="Fridge"
              type="checkbox"
              name="services"
              value="fullFridge"
              onChange={handleToggleService}
            />

            <Input
              id="service-laundry"
              label="Laundry"
              type="checkbox"
              name="services"
              value="laundry"
              onChange={handleToggleService}
            />
          </div>
        </ServicesBlock>
      </SecondFilter>
    </StyledBase>
  )
}

export default SearchFilter
