import styled from 'styled-components'

export const StyledBase = styled.div`
`

export const FirstFilter = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(130px,1fr));
  grid-gap: 10px;
`

export const SecondFilter = styled.div`
  display: flex;
  margin: 20px 0 40px;
`

export const ServicesBlock = styled.div`
  width: 50%;
  > div {
    display: flex;
    > div {
      margin-right: 30px;
      display: flex;
      align-items: center;
      flex-direction: row-reverse;
      label {
        margin-left: 10px;
      }
    }
  }
`
