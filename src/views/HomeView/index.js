import React from 'react';
import {connect} from 'react-redux';

import ApartmentTile from "../../Components/ApartmentTile";
import LoadingSpinner from "../../Components/LoadingSpinner";
import {fetchApartmentsList} from '../../actions/apartmentsListActions';

import { StyleBase, Title } from './style'

class HomeView extends React.Component {
  componentWillMount() {
    this.props.fetchApartmentsList();
  }

  render() {
    let {apartmentsList} = this.props;
    if (apartmentsList.loading) {
      return (
        <div style={{
          marginTop: 160
        }}>
          <LoadingSpinner />
        </div>
      )
    }
    if (apartmentsList.error) {
        return <div>{apartmentsList.error}</div>
    }

    return (
      <StyleBase>
        <Title>Explore Homelike</Title>
        <div className="apartment-list">
          {apartmentsList.items.map((item, index) => (
            <ApartmentTile key={index} apartment={item} />
          ))}
        </div>
      </StyleBase>
    )
  }
}

const mapStateToProps = state => ({
  apartmentsList: {...state.apartmentsList}
});

export default connect(mapStateToProps, {fetchApartmentsList})(HomeView)
