import styled from 'styled-components'

export const StyleBase = styled.div`
  .locations-list {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(290px, 1fr));
    gap: 50px 30px;
  }
`

export const Title = styled.div`
  font-size: 28px;
  font-weight: 700;
  color: ${props => props.theme.secondary};
  margin-bottom: 30px;
`


