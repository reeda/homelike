import React, { Component } from 'react'
import { connect } from 'react-redux'

import { fetchLocations } from '../../actions/locationsActions'
import CityTile from '../../Components/CityTile'
import LoadingSpinner from '../../Components/LoadingSpinner'
import { StyleBase, Title } from './style'

class LocationsView extends Component {
  state = {}

  componentDidMount() {
    this.props.fetchLocations()
  }

  render() {
    let { locationsList } = this.props
    if (locationsList.loading) {
      return (
        <div style={{
          marginTop: 160
        }}>
          <LoadingSpinner />
        </div>
      )
    }
    if (locationsList.error) {
      return <div>{locationsList.error}</div>
    }
    
    return (
      <StyleBase>
        <Title>Live anywhere</Title>
        <div className="locations-list">
          {locationsList.items.map(location => (
            <CityTile key={location._id} {...location} />
          ))}
        </div>
      </StyleBase>
    )
  }
}

const mapStateToProps = state => ({
  locationsList: { ...state.locationsList }
})

export default connect(
  mapStateToProps,
  { fetchLocations }
)(LocationsView)
