import React from 'react'
import { connect } from 'react-redux'

import ApartmentAmentity from '../../Components/ApartmentAmentity'
import LoadingSpinner from '../../Components/LoadingSpinner'
import { fetchApartment } from '../../actions/apartmentActions'

import {
  StyleBase,
  ApartmentCover,
  ApartmentDetails,
  ApartmentTitle,
  ApartmentPrice,
  ApartmentOwner,
  OwnerAvatar,
  OwnerContact,
  ApartmentRooms,
  ApartmentLocation
} from './style'

export class ApartmentView extends React.Component {
  componentWillMount() {
    const {
      match: { params }
    } = this.props
    const { apartmentId } = params
    this.props.fetchApartment(apartmentId)
  }

  render() {
    const { apartment } = this.props
    if (apartment.loading) {
      return (
        <div style={{
          marginTop: 160
        }}>
          <LoadingSpinner />
        </div>
      )
    }
    if (apartment.error) {
      return <div>{apartment.error}</div>
    }
    let image =
      process.env.REACT_APP_HOME_URL +
      '/images/apartments/' +
      apartment.data.images[0]
    const { owner } = apartment.data


    return (
      <StyleBase>
        <ApartmentCover bgImg={image} />
        <ApartmentDetails>
          <ApartmentTitle>{apartment.data.title}</ApartmentTitle>
          <ApartmentLocation>
            <img height="16" src="/img/icon/location.svg" alt={apartment.data.location.title}/>
            <span>{apartment.data.location.title}</span>
          </ApartmentLocation>
          <ApartmentPrice>
            {apartment.data.price}€ <span>/ month</span>
          </ApartmentPrice>

          <ApartmentOwner>
            <OwnerAvatar>{owner.profile.firstName.charAt(0)}</OwnerAvatar>
            <OwnerContact>
              <div className="apartment-owner__name">
                {owner.profile.firstName} {owner.profile.lastName}
              </div>
              <div className="apartment-owner__email">{owner.email}</div>
            </OwnerContact>
          </ApartmentOwner>

          <h3>Beds and rooms</h3>
          <ApartmentRooms>
            <div>
              <img src="/img/icon/size.svg" height="25" alt="size"/>
              <span>{apartment.data.size} m²</span>
            </div>
            {
              Object.keys(apartment.data.details)
              // ignore typename apollo's key
              .filter(key => key !== '__typename')
              .map(key => (
                <div key={`item-room-${key}`}>
                  <img src={`/img/icon/${key}.svg`} height="18" alt=""/>
                  <span>{apartment.data.details[key]}</span>
                </div>
              ))
            }
          </ApartmentRooms>

          <h3>Amenities</h3>
          <div>
            <ApartmentAmentity amenities={apartment.data.amenities} limit="20" />
          </div>
        </ApartmentDetails>
      </StyleBase>
    )
  }
}

const mapStateToProps = state => ({
  apartment: { ...state.apartmentItem }
})

export default connect(
  mapStateToProps,
  { fetchApartment }
)(ApartmentView)
