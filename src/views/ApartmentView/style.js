import styled from 'styled-components'

export const StyleBase = styled.div`
  display: flex;
  h3 {
    font-size: 18px;
    font-weight: 500;
    color: ${props => props.theme.secondary};
    margin: 20px 0 10px;
  }
  min-height: 400px;
`

export const ApartmentCover = styled.div`
  width: calc(60% - 30px);
  background-image: url(${props => props.bgImg});
  background-size: cover;
  margin-right: 30px;
  border-radius: 2px;
`

export const ApartmentDetails = styled.div`
  width: 40%;
  display: flex;
  flex-direction: column;
`

export const  ApartmentTitle = styled.div`
  font-size: 15px;
  color: ${props => props.theme.secondary};
  font-weight: 500;
`

export const ApartmentPrice = styled.div`
  color: ${props => props.theme.primary};
  font-size: 36px;
  margin: 20px 0 30px;
  font-weight: 700;
  span {
    font-weight: 400;
    font-size: 23px;
  }
`

export const ApartmentRooms = styled.div`
 div {
  font-size: 12px;
  font-weight: 500;
  letter-spacing: .2px;
  color: ${props => props.theme.secondary};
  display: inline-flex;
  align-items: center;
  margin-right: 15px;
  span {
    margin-left: 10px;
    text-transform: capitalize;
  }
 }
`

export const ApartmentOwner = styled.div`
  display: flex;
  margin-bottom: auto;
`

export const OwnerContact = styled.div`
  margin-left: 15px;
  .apartment-owner__name {
    color: ${props => props.theme.primary};
    font-weight: 500;
  }
  .apartment-owner__email {
    color: ${props => props.theme.grey};
    font-size: 12px;
    line-height: 12px;
  }
`

export const OwnerAvatar = styled.div`
  height: 40px;
  width: 40px;
  background-color: ${props => props.theme.primary};
  color: #FFF;
  text-transform: uppercase;
  font-size: 22px;
  line-height: 40px;
  text-align: center;
  border-radius: 50%;
`

export const ApartmentLocation = styled.div`
  font-size: 13px;
  font-weight: 500;
  display: flex;
  color: #a3a9af;
  span {
    margin-left: 5px;
  }
`
