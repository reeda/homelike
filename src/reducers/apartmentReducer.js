import {FETCH_APARTMENT, FETCH_APARTMENT_SUCCESS, FETCH_APARTMENT_FAIL} from './../actions/types';

const initialState = {
  data: {},
  loading: true,
  error: null
};


export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_APARTMENT:
            return {
                ...state,
                loading: true,
                error: null
            };
        case FETCH_APARTMENT_SUCCESS:
            return {
                ...state,
                data: action.payload.apartment,
                loading: false,
                error: null
            };
        case FETCH_APARTMENT_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload.error
            };
        default:
            return state;
    }
}
