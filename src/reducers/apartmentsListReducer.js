import {FETCH_APARTMENTS_LIST, FETCH_APARTMENTS_LIST_SUCCESS,FETCH_APARTMENTS_LIST_FAIL} from './../actions/types';

const initialState = {
    items: {},
    loading: true,
    error: null
};


export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_APARTMENTS_LIST:
            return {
                ...state,
                loading: true,
                error: null
            };
        case FETCH_APARTMENTS_LIST_SUCCESS:
            return {
                ...state,
                items: action.payload.apartments.items,
                loading: false,
                errro: null
            };
        case FETCH_APARTMENTS_LIST_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload.error
            };
        default:
            return state;
    }
}
