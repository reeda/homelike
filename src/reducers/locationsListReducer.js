import {FETCH_LOCATIONS, FETCH_LOCATIONS_SUCCESS,FETCH_LOCATIONS_FAIL} from './../actions/types';

const initialState = {
    items: {},
    loading: true,
    error: null
};


export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_LOCATIONS:
            return {
                ...state,
                loading: true,
                error: null
            };
        case FETCH_LOCATIONS_SUCCESS:
            return {
                ...state,
                items: action.payload.locations.items,
                loading: false,
                errro: null
            };
        case FETCH_LOCATIONS_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload.error
            };
        default:
            return state;
    }
}
