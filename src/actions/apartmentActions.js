import {FETCH_APARTMENT, FETCH_APARTMENT_SUCCESS, FETCH_APARTMENT_FAIL} from "./types";
import gql from "graphql-tag";
import client from './../ApolloClient'

export const fetchApartment = (_id) => dispatch => {
  dispatch({
    type: FETCH_APARTMENT
  })
  client.query({
    query: gql`
    {
      apartment(_id: "${_id}") {
        _id
        owner {
          _id
          email
          profile {
            firstName
            lastName
          }
        }
        title
        location {
          title
        }
        size
        price
        images
        amenities
        details {
          rooms
          bedrooms
          floor
          bathrooms
        } 
        services 
      }
    }`
})
.then(apartment => dispatch({
  type: FETCH_APARTMENT_SUCCESS,
  payload: apartment.data
}))
.catch(error => {
  console.log(error)
  dispatch({
    type: FETCH_APARTMENT_FAIL,
    payload: {
      error: 'Opps something went wrong!'
    }
  })
});;
};


