import {FETCH_APARTMENTS_LIST, FETCH_APARTMENTS_LIST_SUCCESS, FETCH_APARTMENTS_LIST_FAIL} from "./types";
import gql from "graphql-tag";
import client from './../ApolloClient'

// if *NO* `location` @param is provided this will fetch all the apartments
export const fetchApartmentsList = ({location} = {}) => dispatch => {
  dispatch({
    type: FETCH_APARTMENTS_LIST
  })
  client.query({
    query: gql`
    {
      apartments(active: true ${!!location ? `location: "${location}"` : ''}) {
        items {
          _id
          owner {
          _id
            email
          } 
          title
          location {
            title
          }
          size
          price
          amenities
          images
          amenities
          details {
            rooms
            bedrooms
            floor
            bathrooms
          } 
          services 
        }
      }
    }`
  })
  .then(apartments => dispatch({
    type: FETCH_APARTMENTS_LIST_SUCCESS,
    payload: apartments.data
  }))
  .catch(error => {
    console.log(error)
    dispatch({
      type: FETCH_APARTMENTS_LIST_FAIL,
      payload: {
        error: 'Opps something went wrong!'
      }
    })
  });
};
