import {FETCH_LOCATIONS, FETCH_LOCATIONS_SUCCESS, FETCH_LOCATIONS_FAIL} from "./types";
import gql from "graphql-tag";
import client from './../ApolloClient'

export const fetchLocations = () => dispatch => {
  dispatch({
    type: FETCH_LOCATIONS
  })
  client.query({
    query: gql`
    {
      locations (active: true) {
        items {
          _id
          title
        }
      }
    }`
})
.then(locations => dispatch({
  type: FETCH_LOCATIONS_SUCCESS,
  payload: locations.data
}))
.catch(error => {
  console.log(error)
  dispatch({
    type: FETCH_LOCATIONS_FAIL,
    payload: {
      error: 'Opps something went wrong!'
    }
  })
});
};


