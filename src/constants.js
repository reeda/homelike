const constants = {
  theme: {
    primary: '#2e587c',
    secondary: 'rgb(76, 91, 92)',
    grey: '#5a636e'
  }
};

export default constants;
